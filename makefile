.PHONY: todo exe

INCLUDES = -I/usr/local/include/SDL2
LIBS = -L/usr/local/lib -lSDL2 -lpthread -D_REENTRANT

todo: main

exe: todo
	-./main

objetos :=

objetos := $(objetos) main.o
main.o: main.cpp
	g++ -std=c++11 -c $(INCLUDES) main.cpp -o main.o

main: $(objetos)
	g++ $(objetos) -o main $(LIBS)
