#include <SDL.h>
#include <iostream>

//#define DEBUG

struct Params{
	bool fullScreen, fullRandom;
	Uint32 w, h, maxFixSpace, nextFixSpace;
	enum{ Blanco, Negro, Random
	}start, border;
	Params():fullScreen(false),w(100),h(100),start(Random),border(Blanco),fullRandom(true),maxFixSpace(1),nextFixSpace(0){}
}params;

class Memoria{
private:
	SDL_Surface *imagen[2];
	Uint32 ptr, Blanco, Negro, Rojo;
	inline Uint32 dis( Uint32 p){ return (++p)%2;}
	inline void setPixel( Uint32 p, Uint32 i, Uint32 j, Uint32 color){
		*(Uint32*)((Uint8*)(this->imagen[p]->pixels) + j*this->imagen[p]->pitch + i*4) = color;
	}
	inline Uint32 surround( Uint32 p, Uint32 i, Uint32 j){
		Uint32 surroundings = 0;
		if( *(Uint32*)((Uint8*)(this->imagen[p]->pixels) + (j-1)*this->imagen[p]->pitch + (i-1)*4) == this->Negro) surroundings++;
		if( *(Uint32*)((Uint8*)(this->imagen[p]->pixels) + (j-1)*this->imagen[p]->pitch + (i)*4) == this->Negro) surroundings++;
		if( *(Uint32*)((Uint8*)(this->imagen[p]->pixels) + (j-1)*this->imagen[p]->pitch + (i+1)*4) == this->Negro) surroundings++;
		if( *(Uint32*)((Uint8*)(this->imagen[p]->pixels) + (j)*this->imagen[p]->pitch + (i-1)*4) == this->Negro) surroundings++;
		if( *(Uint32*)((Uint8*)(this->imagen[p]->pixels) + (j)*this->imagen[p]->pitch + (i+1)*4) == this->Negro) surroundings++;
		if( *(Uint32*)((Uint8*)(this->imagen[p]->pixels) + (j+1)*this->imagen[p]->pitch + (i-1)*4) == this->Negro) surroundings++;
		if( *(Uint32*)((Uint8*)(this->imagen[p]->pixels) + (j+1)*this->imagen[p]->pitch + (i)*4) == this->Negro) surroundings++;
		if( *(Uint32*)((Uint8*)(this->imagen[p]->pixels) + (j+1)*this->imagen[p]->pitch + (i+1)*4) == this->Negro) surroundings++;
#ifdef DEBUG
std::cout << "Negro: " << this->Negro << ", Blanco: " << this->Blanco << ", Rojo: " << this->Rojo << std::endl;
std::cout << "i-1, j-1: " << *(Uint32*)((Uint8*)(this->imagen[p]->pixels) + (j-1)*this->imagen[p]->pitch + (i-1)*4) << std::endl;
std::cout << "i, j-1: " << *(Uint32*)((Uint8*)(this->imagen[p]->pixels) + (j-1)*this->imagen[p]->pitch + (i)*4) << std::endl;
std::cout << "i+1, j-1: " << *(Uint32*)((Uint8*)(this->imagen[p]->pixels) + (j-1)*this->imagen[p]->pitch + (i+1)*4) << std::endl;
std::cout << "i-1, j: " << *(Uint32*)((Uint8*)(this->imagen[p]->pixels) + (j)*this->imagen[p]->pitch + (i-1)*4) << std::endl;
std::cout << "i+1, j: " << *(Uint32*)((Uint8*)(this->imagen[p]->pixels) + (j)*this->imagen[p]->pitch + (i+1)*4) << std::endl;
std::cout << "i-1, j+1: " << *(Uint32*)((Uint8*)(this->imagen[p]->pixels) + (j+1)*this->imagen[p]->pitch + (i-1)*4) << std::endl;
std::cout << "i, j+1: " << *(Uint32*)((Uint8*)(this->imagen[p]->pixels) + (j+1)*this->imagen[p]->pitch + (i)*4) << std::endl;
std::cout << "i+1, j+1: " << *(Uint32*)((Uint8*)(this->imagen[p]->pixels) + (j+1)*this->imagen[p]->pitch + (i+1)*4) << std::endl;
#endif//DEBUG
		return surroundings;
	}
	inline Uint32 life( Uint32 p, Uint32 i, Uint32 j){
		Uint32 ret = this->Blanco, s = surround( p, i, j);
		if( s == 3) ret = this->Negro;
		else if( s == 2 && *(Uint32*)((Uint8*)(this->imagen[p]->pixels) + j*this->imagen[p]->pitch + i*4) == this->Negro)
			ret = this->Negro;
		return ret;
	}
	inline Uint32 randColor(){
		return (rand()%2) == 0 ? this->Blanco : this->Negro;
	}
public:
	Memoria( Uint32 w, Uint32 h);
	void update();
	inline SDL_Surface *get(){ return this->imagen[this->ptr];}
};

int main(int argc, char *argv[]){
	SDL_Window *ventana;
	SDL_Renderer *render;
	SDL_Texture *textura;
	SDL_Event evento;
	Sint32 W,H, T = 0;
	bool salida = false, pause = false;

	SDL_Init(SDL_INIT_VIDEO);
	ventana = SDL_CreateWindow("Ventana", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,0,0,SDL_WINDOW_FULLSCREEN_DESKTOP);
	if( params.fullScreen)
		SDL_GetWindowSize(ventana,&W,&H);
	else{
		W = params.w;
		H = params.h;
	}
	render = SDL_CreateRenderer(ventana,-1,0);
	SDL_RenderSetLogicalSize(render, W,H);
	textura = SDL_CreateTexture(render,SDL_PIXELFORMAT_ARGB8888,SDL_TEXTUREACCESS_STREAMING,W,H);

	Memoria *memoria = new Memoria( W, H);

	srand( SDL_GetTicks());

	while(!salida){
		SDL_UpdateTexture(textura,nullptr,memoria->get()->pixels,memoria->get()->pitch);
		SDL_RenderClear(render);
		SDL_RenderCopy(render,textura,nullptr,nullptr);
		SDL_RenderPresent(render);

		while(SDL_PollEvent(&evento)) switch(evento.type){
		case SDL_QUIT:
			salida = true;
			break;
		case SDL_KEYDOWN:
			switch(evento.key.keysym.sym){
			case SDLK_q:
				salida = true;
				break;
			case SDLK_p:
				pause = !pause;
				break;
			case SDLK_PLUS:
				if( params.maxFixSpace * 2 > params.maxFixSpace)
					params.maxFixSpace *= 2;
				break;
			case SDLK_MINUS:
				if( params.maxFixSpace > 1)
					params.maxFixSpace /= 2;
				break;
			default:
				T++;
			}
			break;
		default:;
		}

		if( pause){
			if(T > 0){ T--; memoria->update();}
		}else
			memoria->update();

		params.maxFixSpace += 1;
		srand( SDL_GetTicks());
		SDL_Delay(30);
	}

	SDL_Quit();
	return 0;
}

Memoria::Memoria( Uint32 w, Uint32 h):ptr(0){
	Uint32 i, j, tmp;
	this->imagen[this->ptr] = SDL_CreateRGBSurface(0,w,h,32,0,0,0,0);
	this->imagen[this->dis(this->ptr)] = SDL_CreateRGBSurface(0,w,h,32,0,0,0,0);
	this->Blanco = SDL_MapRGB( this->imagen[this->ptr]->format, 0xff, 0xff, 0xff);
	this->Negro = SDL_MapRGB( this->imagen[this->ptr]->format, 0x0, 0x0, 0x0);
	this->Rojo = SDL_MapRGB( this->imagen[this->ptr]->format, 0xff, 0x0, 0x0);
	if( params.start == params.Random){
		for(i = 0; i < this->imagen[this->ptr]->w; i++)
			for(j = 0; j < this->imagen[this->ptr]->h; j++){
				this->setPixel( this->ptr, i, j, this->randColor());
		}
		for(i = 0; i < this->imagen[this->dis( this->ptr)]->w; i++)
			for(j = 0; j < this->imagen[this->dis( this->ptr)]->h; j++){
				this->setPixel( this->dis( ptr), i, j, this->randColor());
		}
	} else{
		tmp = (params.start == params.Blanco) ? this->Blanco : this->Negro;
		SDL_Rect rect = { 0, 0, w, h};
		SDL_FillRect( this->imagen[this->ptr], &rect, tmp);
		SDL_FillRect( this->imagen[this->dis(this->ptr)], &rect, tmp);
	}
}

void Memoria::update(){
	Uint32 i,j, tmp;
	SDL_Surface *front = this->imagen[ this->ptr];
	SDL_Surface *back = this->imagen[ this->dis( this->ptr)];
	SDL_Rect rect = { 0, 0, back->w, back->h};
	SDL_FillRect( back, &rect, this->Rojo);
	if( params.border == params.Random){
		for(i = 0; i < front->w; i++){
			this->setPixel( this->ptr, i, 0, this->randColor());
			this->setPixel( this->ptr, i, front->h-1, this->randColor());
		}
		for(i = 0; i < front->h; i++){
			this->setPixel( this->ptr, 0, i, this->randColor());
			this->setPixel( this->ptr, front->w-1, i, this->randColor());
		}
	} else{
		tmp = (params.border == params.Blanco) ? this->Blanco : this->Negro;
		for(i = 0; i < front->w; i++){
			this->setPixel( this->ptr, i, 0, tmp);
			this->setPixel( this->ptr, i, front->h-1, tmp);
		}
		for(i = 0; i < front->h; i++){
			this->setPixel( this->ptr, 0, i, tmp);
			this->setPixel( this->ptr, front->w-1, i, tmp);
		}		
	}
	for(i = 1; i < front->w-1; i++)
		for(j = 1; j < front->h-1; j++){
			if( params.fullRandom)
				if( params.nextFixSpace-- < 1){
					params.nextFixSpace = rand() % params.maxFixSpace;
					tmp = this->randColor();
				} else
					tmp = this->life( this->ptr, i, j);
			else
				tmp = this->life( this->ptr, i, j);
			this->setPixel( this->dis( this->ptr), i, j, tmp);
	}
	this->ptr = this->dis( this->ptr);
}

